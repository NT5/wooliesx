﻿using Exercises.Models;
using Exercises.Services;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Exercises.Test
{
    public class ResourceServiceTest
    {
        ResourceService ArrangeService(string mockContent)
        {
            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(mockContent),
                });

            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(x => x.CreateClient(It.IsAny<string>())).Returns(client);

            var mockLoger = new Mock<ILogger<ResourceService>>();

            var inMemorySettings = new Dictionary<string, string> {
                {"ResourceUrl", "http://localhost/"},
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            return new ResourceService(configuration, mockFactory.Object, mockLoger.Object);
        }

        [Fact]
        public async void GetResource_User_UserReturned()
        {
            var userModel = new User
            {
                Name = "testName",
                Token = "testToken"
            };

            var resourceService = ArrangeService(JsonSerializer.Serialize(userModel));

            var user = await resourceService.GetResourceAsync<User>(string.Empty);

            user.Should().NotBe(userModel);
            user.Should().BeEquivalentTo(userModel);
        }

        [Fact]
        public async void GetResource_Products_ProductsReturned()
        {
            var productsModel = new List<Product>
            {
                new Product { Name="E", Price = 5, Quantity = 5 },
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="D", Price = 30, Quantity = 4 },
            };

            var resourceService = ArrangeService(JsonSerializer.Serialize(productsModel));

            var products = await resourceService.GetResourceAsync<List<Product>>(string.Empty);

            products.Should().NotBeSameAs(productsModel);
            products.Should().BeEquivalentTo(productsModel);
        }

        [Fact]
        public async void GetResource_TrolleyTotal_ValueReturned()
        {
            var resourceService = ArrangeService("1");

            var total = await resourceService.GetMinTrolleyTotalAsync(new TrolleyTotalInput());

            total.Should().Be(1M);
        }
    }
}
