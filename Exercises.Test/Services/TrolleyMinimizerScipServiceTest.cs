﻿using Exercises.Services;
using Xunit;
using Moq;
using Microsoft.Extensions.Logging;
using Exercises.Models;
using System.Collections.Generic;
using FluentAssertions;
using System;

namespace Exercises.Test
{
    public class TrolleyMinimizerScipServiceTest
    {
        const decimal eps = 1e-20M;
        protected TrolleyMinimizerScipService service;

        public TrolleyMinimizerScipServiceTest()
        {
            var mockLoger = new Mock<ILogger<TrolleyMinimizerScipService>>();
            service = new TrolleyMinimizerScipService(mockLoger.Object);
        }

        [Fact]
        public async void TotalScip_InputWithoutSpecials_Optimized()
        {
            var input = new TrolleyTotalInput
            {
                Products = new List<Product>
                {
                    new Product{ Name = "A", Price = 10 },
                    new Product{ Name = "B", Price = 20 },
                },
                Specials = new List<Special>(),
                Quantities = new List<ProductQuantity>
                {
                    new ProductQuantity { Name = "A", Quantity = 10 },
                    new ProductQuantity { Name = "B", Quantity = 5 },
                }
            };

            var result = await service.CalculateMinTotalAsync(input);

            result.Should().BeApproximately(200M, eps);
        }

        [Fact]
        public void TotalScip_InputWithoutPrice_ExceptionThrown()
        {
            var input = new TrolleyTotalInput
            {
                Products = new List<Product>(),
                Specials = new List<Special>(),
                Quantities = new List<ProductQuantity>
                {
                    new ProductQuantity { Name = "A", Quantity = 10 },
                }
            };

            service.Invoking(async x => await x.CalculateMinTotalAsync(input)).Should().Throw<Exception>();
        }

        [Fact]
        public async void TotalScip_SimpleInput01_Optimized()
        {
            var input = new TrolleyTotalInput
            {
                Products = new List<Product>()
                {
                    new Product{ Name = "A", Price = 10 },
                },
                Specials = new List<Special>()
                {
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 1 }
                        },
                        Total = 9
                    }
                },
                Quantities = new List<ProductQuantity>
                {
                    new ProductQuantity { Name = "A", Quantity = 10 },
                }
            };

            var result = await service.CalculateMinTotalAsync(input);

            result.Should().BeApproximately(90M, eps);
        }

        [Fact]
        public async void TotalScip_SimpleInput02_Optimized()
        {
            var input = new TrolleyTotalInput
            {
                Products = new List<Product>
                {
                    new Product{ Name = "A", Price = 10 },
                    new Product{ Name = "B", Price = 20 },
                },
                Specials = new List<Special>
                {
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 2 },
                            new ProductQuantity { Name = "B", Quantity = 3 },
                        },
                        Total = 50
                    },
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 1 },
                            new ProductQuantity { Name = "B", Quantity = 1 },
                        },
                        Total = 20
                    }
                },
                Quantities = new List<ProductQuantity>
                {
                    new ProductQuantity { Name = "A", Quantity = 10 },
                    new ProductQuantity { Name = "B", Quantity = 5 },
                }
            };

            var result = await service.CalculateMinTotalAsync(input);

            result.Should().BeApproximately(150M, eps);
        }

        [Fact]
        public async void TotalScip_SimpleInput03_Optimized()
        {
            var input = new TrolleyTotalInput
            {
                Products = new List<Product>
                {
                    new Product{ Name = "A", Price = 10 },
                    new Product{ Name = "B", Price = 20 },
                    new Product{ Name = "C", Price = 30 },
                },
                Specials = new List<Special>
                {
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 2 },
                            new ProductQuantity { Name = "B", Quantity = 3 },
                            new ProductQuantity { Name = "C", Quantity = 1 },
                        },
                        Total = 50
                    },
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 1 },
                            new ProductQuantity { Name = "B", Quantity = 1 },
                            new ProductQuantity { Name = "C", Quantity = 1 },
                        },
                        Total = 40
                    },
                    new Special
                    {
                        Quantities = new List<ProductQuantity>
                        {
                            new ProductQuantity { Name = "A", Quantity = 3 },
                            new ProductQuantity { Name = "C", Quantity = 1 },
                        },
                        Total = 20
                    }
                },
                Quantities = new List<ProductQuantity>
                {
                    new ProductQuantity { Name = "A", Quantity = 10 },
                    new ProductQuantity { Name = "B", Quantity = 5 },
                    new ProductQuantity { Name = "C", Quantity = 5 },
                }
            };

            var result = await service.CalculateMinTotalAsync(input);

            result.Should().BeApproximately(170M, eps);
        }
    }
}
