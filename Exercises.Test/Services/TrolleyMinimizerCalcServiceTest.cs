﻿using Exercises.Services;
using Xunit;
using Moq;
using Exercises.Models;
using FluentAssertions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Exercises.Test
{
    public class TrolleyMinimizerCalcServiceTest
    {
        [Fact]
        public async void TotalCalc_AnyInput_ValueReturned()
        {
            var mockResource = new Mock<IResourceService>();
            mockResource.Setup(x => x.GetMinTrolleyTotalAsync(It.IsAny<TrolleyTotalInput>())).Returns(Task.FromResult(1M));
            var mockLoger = new Mock<ILogger<TrolleyMinimizerCalcService>>();
            var service = new TrolleyMinimizerCalcService(mockResource.Object, mockLoger.Object);

            var total = await service.CalculateMinTotalAsync(new TrolleyTotalInput());

            total.Should().Be(1M);
        }
    }
}
