using Exercises.Models;
using Exercises.Services;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Microsoft.Extensions.Logging;

namespace Exercises.Test
{
    public class ProductServiceTest
    {
        readonly ProductService productService;
        readonly List<Customer> customers;
        readonly List<Product> products;

        public ProductServiceTest()
        {
            customers = new()
            {
                new Customer
                {
                    CustomerId = 1,
                    Products = new List<Product>
                    {
                        new Product { Name="A", Price = 10, Quantity = 1 },
                        new Product { Name="B", Price = 20, Quantity = 5 },
                        new Product { Name="C", Price = 15, Quantity = 2 },
                    }
                },
                new Customer
                {
                    CustomerId = 2,
                    Products = new List<Product>
                    {
                        new Product { Name="A", Price = 10, Quantity = 6 },
                        new Product { Name="B", Price = 20, Quantity = 1 },
                        new Product { Name="D", Price = 30, Quantity = 4 },
                    }
                }
            };

            products = new()
            {
                new Product { Name = "A", Price = 10, Quantity = 1 },
                new Product { Name = "B", Price = 20, Quantity = 2 },
                new Product { Name = "C", Price = 15, Quantity = 3 },
                new Product { Name = "D", Price = 30, Quantity = 4 },
                new Product { Name = "E", Price = 5, Quantity = 5 },
            };

            var mockLoger = new Mock<ILogger<ProductService>>();
            var mockResourceService = new Mock<IResourceService>();
            mockResourceService.Setup(x => x.GetResourceAsync<List<Customer>>("shopperHistory")).Returns(Task.FromResult(customers));
            mockResourceService.Setup(x => x.GetResourceAsync<List<Product>>("products")).Returns(Task.FromResult(products));

            productService = new ProductService(mockResourceService.Object, mockLoger.Object);
        }

        [Fact]
        public async void Products_SortLow_Sorted()
        {
            var productsSorted = new List<Product> 
            {
                new Product { Name="E", Price = 5, Quantity = 5 },
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="D", Price = 30, Quantity = 4 },
            };

            await productService.SortProductsAsync(products, Enums.SortingOptions.Low);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_SortHigh_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="D", Price = 30, Quantity = 4 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="E", Price = 5, Quantity = 5 },
            };

            await productService.SortProductsAsync(products, Enums.SortingOptions.High);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_SortAscending_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="D", Price = 30, Quantity = 4 },
                new Product { Name="E", Price = 5, Quantity = 5 },
            };

            await productService.SortProductsAsync(products, Enums.SortingOptions.Ascending);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_SortDescending_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="E", Price = 5, Quantity = 5 },
                new Product { Name="D", Price = 30, Quantity = 4 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="A", Price = 10, Quantity = 1 },
            };

            await productService.SortProductsAsync(products, Enums.SortingOptions.Descending);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_SortRecommended_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="D", Price = 30, Quantity = 4 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="E", Price = 5, Quantity = 5 },
            };

            await productService.SortProductsAsync(products, Enums.SortingOptions.Recommended);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_SortRecommendedMethod_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="D", Price = 30, Quantity = 4 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="E", Price = 5, Quantity = 5 },
            };

            await productService.SortRecommendedAsync(products);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }

        [Fact]
        public async void Products_GetSortedProductsMethod_Sorted()
        {
            var productsSorted = new List<Product>
            {
                new Product { Name="E", Price = 5, Quantity = 5 },
                new Product { Name="A", Price = 10, Quantity = 1 },
                new Product { Name="C", Price = 15, Quantity = 3 },
                new Product { Name="B", Price = 20, Quantity = 2 },
                new Product { Name="D", Price = 30, Quantity = 4 },
            };

            var products = await productService.GetSortedProductsAsync(Enums.SortingOptions.Low);

            products.Should().BeEquivalentTo(productsSorted, options => options.WithStrictOrdering());
        }
    }
}
