﻿namespace Exercises.Enums
{
    public enum SortingOptions
    {
        Low,
        High,
        Ascending,
        Descending,
        Recommended,
    }
}
