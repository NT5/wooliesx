﻿using Exercises.Models;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public interface ITrolleyMinimizerService
    {
        Task<decimal> CalculateMinTotalAsync(TrolleyTotalInput input);
    }
}
