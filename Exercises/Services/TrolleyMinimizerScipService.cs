﻿using Exercises.Models;
using Google.OrTools.LinearSolver;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public class TrolleyMinimizerScipService : ITrolleyMinimizerService
    {
        private readonly ILogger<TrolleyMinimizerScipService> logger;

        private Solver solver;
        private List<SpecialScip> specials;
        private List<ProductQuantityScip> quantities;
        private Dictionary<string, decimal> prices;

        public TrolleyMinimizerScipService(ILogger<TrolleyMinimizerScipService> logger)
        {
            this.logger = logger;
        }

        public Task<decimal> CalculateMinTotalAsync(TrolleyTotalInput input)
        {
            logger.LogInformation($"Execution of {nameof(CalculateMinTotalAsync)}");

            Initialize(input);

            AddConstraints();

            var success = MinimizeTotal();
            if (!success)
            {
                throw new Exception("The problem does not have an optimal solution!");
            }

            var minTotal = RecalculateTotal();

            return Task.FromResult(minTotal);
        }

        private void Initialize(TrolleyTotalInput input)
        {
            solver = Solver.CreateSolver("SCIP");

            specials = input.Specials.Select((special, id) =>
            {
                var countVar = solver.MakeIntVar(0, int.MaxValue, $"S{id}");
                return new SpecialScip(special, countVar);
            })
            .ToList();

            quantities = input.Quantities.Select((quantity, id) =>
            {
                var countVar = solver.MakeIntVar(0, int.MaxValue, $"P{id}");
                return new ProductQuantityScip(quantity, countVar);
            })
            .ToList();

            prices = input.Products.ToDictionary(x => x.Name, x => x.Price);
        }

        private void AddConstraints()
        {
            foreach (var quantity in quantities)
            {
                var prodQuantExpr = specials.Select(special => new
                {
                    ProdQuant = special.Quantities.SingleOrDefault(x => x.Name == quantity.Name),
                    Special = special,
                })
                .Where(x => x.ProdQuant != null)
                .Aggregate(quantity.CountVar * 1, (total, x) => total + x.Special.CountVar * (double)x.ProdQuant.Quantity);

                solver.Add(prodQuantExpr <= (double)quantity.Quantity + 1e-8);
                solver.Add(prodQuantExpr >= (double)quantity.Quantity - 1e-8);
            }
        }

        private bool MinimizeTotal()
        {
            var priceSimpleExpr = quantities.Aggregate(new LinearExpr(),
                (total, quantity) => total + quantity.CountVar * (double)prices[quantity.Name]);

            var priceSpecialExpr = specials.Aggregate(new LinearExpr(),
                (total, special) => total + special.CountVar * (double)special.Total);

            solver.Minimize(priceSimpleExpr + priceSpecialExpr);
            var resultStatus = solver.Solve();

            return resultStatus == Solver.ResultStatus.OPTIMAL;
        }

        private decimal RecalculateTotal()
        {
            var simpleTotal = quantities.Sum(quantity => (decimal)quantity.CountVar.SolutionValue() * prices[quantity.Name]);
            var specialTotal = specials.Sum(special => (decimal)special.CountVar.SolutionValue() * special.Total);

            return simpleTotal + specialTotal;
        }
    }
}
