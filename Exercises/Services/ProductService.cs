﻿using Exercises.Enums;
using Exercises.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public class ProductService : IProductService
    {
        private readonly IResourceService resourceService;
        private readonly ILogger<ProductService> logger;

        public ProductService(IResourceService resourceService, ILogger<ProductService> logger)
        {
            this.resourceService = resourceService;
            this.logger = logger;
        }

        public async Task SortProductsAsync(List<Product> products, SortingOptions sortOption)
        {
            switch (sortOption)
            {
                case SortingOptions.Ascending:
                    products.Sort((a, b) => +a.Name.CompareTo(b.Name));
                    break;

                case SortingOptions.Descending:
                    products.Sort((a, b) => -a.Name.CompareTo(b.Name));
                    break;

                case SortingOptions.Low:
                    products.Sort((a, b) => +a.Price.CompareTo(b.Price));
                    break;

                case SortingOptions.High:
                    products.Sort((a, b) => -a.Price.CompareTo(b.Price));
                    break;

                case SortingOptions.Recommended:
                    await SortRecommendedAsync(products);
                    break;

                default:
                    throw new Exception($"No sorting method for {sortOption} found");
            }
        }

        public async Task SortRecommendedAsync(List<Product> products)
        {
            var shopperHist = await resourceService.GetResourceAsync<List<Customer>>("shopperHistory");

            var productTotalQuantities = shopperHist
                .SelectMany(x => x.Products)
                .GroupBy(x => x.Name)
                .ToDictionary(x => x.Key, x => x.Sum(x => x.Quantity));

            products.Sort((a, b) =>
            {
                productTotalQuantities.TryGetValue(a.Name, out var totalQuantityA);
                productTotalQuantities.TryGetValue(b.Name, out var totalQuantityB);

                return totalQuantityB.CompareTo(totalQuantityA);
            });
        }

        public async Task<List<Product>> GetSortedProductsAsync(SortingOptions sortOption)
        {
            logger.LogInformation($"Execution of {nameof(GetSortedProductsAsync)}");

            var products = await resourceService.GetResourceAsync<List<Product>>("products");
            await SortProductsAsync(products, sortOption);

            return products;
        }
    }
}
