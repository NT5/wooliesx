﻿using Exercises.Models;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public interface IResourceService
    {
        Task<T> GetResourceAsync<T>(string path);
        Task<decimal> GetMinTrolleyTotalAsync(TrolleyTotalInput input);
    }
}
