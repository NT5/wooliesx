﻿using Exercises.Models;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public class TrolleyMinimizerCalcService : ITrolleyMinimizerService
    {
        private readonly IResourceService resourceService;
        private readonly ILogger<TrolleyMinimizerCalcService> logger;

        public TrolleyMinimizerCalcService(IResourceService resourceService, ILogger<TrolleyMinimizerCalcService> logger)
        {
            this.resourceService = resourceService;
            this.logger = logger;
        }

        public async Task<decimal> CalculateMinTotalAsync(TrolleyTotalInput input)
        {
            logger.LogInformation($"Execution of {nameof(CalculateMinTotalAsync)}");

            return await resourceService.GetMinTrolleyTotalAsync(input);
        }
    }
}
