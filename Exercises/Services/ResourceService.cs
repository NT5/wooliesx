﻿using Exercises.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public class ResourceService : IResourceService
    {
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ILogger<ResourceService> logger;

        public ResourceService(IConfiguration configuration, IHttpClientFactory httpClientFactory, ILogger<ResourceService> logger)
        {
            this.configuration = configuration;
            this.httpClientFactory = httpClientFactory;
            this.logger = logger;
        }

        public async Task<T> GetResourceAsync<T>(string path)
        {
            logger.LogInformation($"Getting resource {nameof(T)}");

            var resourceUrl = configuration.GetValue<string>("ResourceUrl");
            var token = configuration.GetValue<string>("User:Token");

            var httpClient = httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync(resourceUrl + $"{path}?token={token}");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<T>(responseBody, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        }

        public async Task<decimal> GetMinTrolleyTotalAsync(TrolleyTotalInput input)
        {
            logger.LogInformation($"Getting resource {nameof(TrolleyTotalInput)}");

            var resourceUrl = configuration.GetValue<string>("ResourceUrl");
            var token = configuration.GetValue<string>("User:Token");

            var httpClient = httpClientFactory.CreateClient();
            var postContent = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(resourceUrl + $"trolleyCalculator?token={token}", postContent);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

            return decimal.Parse(responseBody, CultureInfo.InvariantCulture);
        }
    }
}
