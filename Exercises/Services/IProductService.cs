﻿using Exercises.Enums;
using Exercises.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exercises.Services
{
    public interface IProductService
    {
        Task<List<Product>> GetSortedProductsAsync(SortingOptions sortOption);
    }
}
