﻿using Exercises.Enums;
using Exercises.Models;
using Exercises.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Exercises.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class AnswersController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly IProductService productService;
        private readonly ITrolleyMinimizerService trolleyMinimizerService;

        public AnswersController(IConfiguration configuration, IProductService productService, ITrolleyMinimizerService trolleyMinimizerService)
        {
            this.configuration = configuration;
            this.productService = productService;
            this.trolleyMinimizerService = trolleyMinimizerService;
        }

        [HttpGet("user")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(User))]
        public IActionResult GetUser()
        {
            var user = configuration.GetSection("User").Get<User>();

            return Ok(user);
        }

        [HttpGet("sort")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<Product>))]
        public async Task<IActionResult> GetSortAsync([FromQuery] SortingOptions sortOption)
        {
            var products = await productService.GetSortedProductsAsync(sortOption);

            return Ok(products);
        }

        [HttpPost("trolleyTotal")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(decimal))]
        public async Task<IActionResult> GetTrolleyTotalAsync([FromBody] TrolleyTotalInput input)
        {
            var minTotal = await trolleyMinimizerService.CalculateMinTotalAsync(input);
         
            return Ok(minTotal);
        }
    }
}
