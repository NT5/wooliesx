﻿using System.Collections.Generic;

namespace Exercises.Models
{
    public class TrolleyTotalInput
    {
        public ICollection<Product> Products { get; set; }
        public ICollection<Special> Specials { get; set; }
        public ICollection<ProductQuantity> Quantities { get; set; }
    }
}