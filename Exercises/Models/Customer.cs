﻿using System.Collections.Generic;

namespace Exercises.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
