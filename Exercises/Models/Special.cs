﻿using System.Collections.Generic;

namespace Exercises.Models
{
    public class Special
    {
        public ICollection<ProductQuantity> Quantities { get; set; }
        public decimal Total { get; set; }
    }
}
