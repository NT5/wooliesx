﻿using Google.OrTools.LinearSolver;

namespace Exercises.Models
{
    public class ProductQuantityScip
    {
        public string Name { get; }
        public decimal Quantity { get; }
        public Variable CountVar { get; }

        public ProductQuantityScip(ProductQuantity productQuantity, Variable countVar)
        {
            Name = productQuantity.Name;
            Quantity = productQuantity.Quantity;
            CountVar = countVar;
        }
    }
}
