﻿using Google.OrTools.LinearSolver;
using System.Collections.Generic;

namespace Exercises.Models
{
    public class SpecialScip
    {
        public ICollection<ProductQuantity> Quantities { get; }
        public decimal Total { get; }
        public Variable CountVar { get; }

        public SpecialScip(Special special, Variable countVar)
        {
            Quantities = special.Quantities;
            Total = special.Total;
            CountVar = countVar;
        }
    }
}
