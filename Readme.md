# WooliesX Exercises

Author: Anatolii Pavlov

Base Url <http://91.245.226.78:81/api/v1/answers>

## Exercise 1

This will call your api looking for a resource available at your base url /user. For example, if the url you request is "http://localhost:5001/api/answers" this will make a GET request to "http://localhost:5001/api/answers/user" The result will be a JSON object in the format {"name": "test", "token" : "1234-455662-22233333-3333"}

Url <http://91.245.226.78:81/api/v1/answers/user>

## Exercise 2

This will call your api looking for a range of different sorting options at your base url /sort Your Api needs to call the "products" resource to get a list of available products This endpoint will need to accept a query string parameter called "sortOption" which will take in the following strings - "Low" - Low to High Price - "High" - High to Low Price - "Ascending" - A - Z sort on the Name - "Descending" - Z - A sort on the Name - "Recommended" - this will call the "shopperHistory" resource to get a list of customers orders and needs to return based on popularity, Your response will be in the same data structure as the "products" response (only sorted correctly)

Url <http://91.245.226.78:81/api/v1/answers/sort>

## Exercise 3

This will call your api looking for a resource available at your base url /trolleyTotal which should return the lowest possible total based on provided lists of prices, specials and quantities.

Url <http://91.245.226.78:81/api/v1/answers/trolleyTotal>

## Extra for Experts

Once you have completed all exercises sucessfully, try the Trolley exercise (/Exercise/exercise3) again but without relying on the /resource/trolleyCalculator api.

Url <http://91.245.226.78:82/api/v1/answers/trolleyTotal>
